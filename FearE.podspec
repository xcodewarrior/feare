Pod::Spec.new do |s|
  s.name             = 'FearE'
  s.version          = '1.0.0'
  s.summary          = 'Fear E is simple joy project'

  s.description      = 'simple description for eazy e and others for real'
  s.homepage         = 'https://gitlab.com/xcodewarrior/feare'
  s.license          = 'MIT'
  s.author           = { 'eliakorkmaz' => 'emrahkrkmz1@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/xcodewarrior/feare.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.source_files     =   'Sources/**/*.swift'
  s.swift_version = "5.0"
end
