# FearE

[![CI Status](https://img.shields.io/travis/eliakorkmaz/FearE.svg?style=flat)](https://travis-ci.org/eliakorkmaz/FearE)
[![Version](https://img.shields.io/cocoapods/v/FearE.svg?style=flat)](https://cocoapods.org/pods/FearE)
[![License](https://img.shields.io/cocoapods/l/FearE.svg?style=flat)](https://cocoapods.org/pods/FearE)
[![Platform](https://img.shields.io/cocoapods/p/FearE.svg?style=flat)](https://cocoapods.org/pods/FearE)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FearE is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FearE'
```

## Author

eliakorkmaz, emrahkrkmz1@gmail.com

## License

FearE is available under the MIT license. See the LICENSE file for more info.
